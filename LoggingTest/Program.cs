﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LoggingFramework;

namespace LoggingTest
{
    class Program
    {
        static void Main(string[] args)
        {
            // Hierachy and Write Test
            {
                Logger logger = new Logger();

                using (var tcase = new TestCase(logger, "SysTC_Test_1", "Test Test Case", "A test case to test casing of test cases"))
                {
                    using (var scope = new TestScope(logger, "Test Test Scope", "A scope to test the scoping of scopes.", false))
                    {
                        logger.Log(new TestStep(TestStepVerdict.Pass, TestStepCategory.Primary, "Test Test Step", "A test step to test stepping of test steps."));
                    }
                }

                logger.Log(new TestStep(TestStepVerdict.Pass, TestStepCategory.Primary, "Unscopey Test Step", "A test step to test unscoping of test steps."));

                var xmlRenderer = new XmlLogRenderer("report.xml");
                xmlRenderer.Render(logger);

                var rawRenderer = new RawLogRenderer("raw.log");
                rawRenderer.Render(logger);
            }

            // Read Test
            {
                Logger logger = new Logger();

                var rawReader = new RawLogReader("raw.log");
                rawReader.Read(logger);

                var rawRenderer = new RawLogRenderer("rawOutput.log");
                rawRenderer.Render(logger);
            }
        }
    }
}
