﻿using System.Collections.Generic;

namespace LoggingFramework
{
    public class Logger
    {
        public Logger()
        {
            LogArray = new LinkedList<LogItem>();
        }

        public void Log(LogItem log_item)
        {
            LogArray.AddLast(log_item);
        }

        public LinkedList<LogItem> LogArray { get; set; }
    }
}
