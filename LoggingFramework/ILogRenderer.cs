﻿namespace LoggingFramework
{
    interface ILogRenderer
    {
        void Render(Logger logger);
    }
}
