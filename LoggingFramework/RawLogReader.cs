﻿using System;
using System.IO;
using System.Linq;

namespace LoggingFramework
{
    public class RawLogReader
    {
        public RawLogReader(string filepath)
        {
            _filepath = filepath;
        }

        public void Read(Logger logger)
        {
            _sr = new StreamReader(_filepath);
            _prevHierachy = 0;
            logger.LogArray.Clear();

            string line;

            // Skip header line
            line = _sr.ReadLine();

            while((line = _sr.ReadLine()) != null)
            {
                logger.LogArray.AddLast(ReadLogItem(line));
            }

            _sr.Dispose();
        }

        public LogItem ReadLogItem(string line)
        {
            var entries = line.Split('|');

            // TODO Determine specific type of LogItem from "Name" field
            LogItem logItem = new GenericLogItem();
            

            int currHierarchy = Int32.Parse(entries[1]);

            logItem.Time = Int32.Parse(entries[0]);
            logItem.Hierarchy = (LogHierarchy) (currHierarchy - _prevHierachy);
            logItem.Name = entries[2];
            logItem.Title = entries[3];
            logItem.Desc = entries[4];

            // Parse Non-Standard Attributes
            try
            {
                logItem.NonStandardAttributes = entries[5].Split(',')
                    .Select(x => x.Split('='))
                    .ToDictionary(x => x[0], x => x[1]);
            }
            catch
            {
                logItem.NonStandardAttributes.Clear();
            }

            // Store the current hierarchy level for next time
            _prevHierachy = currHierarchy;

            return logItem;
        }

        string _filepath;
        StreamReader _sr;

        int _prevHierachy;
    }
}
