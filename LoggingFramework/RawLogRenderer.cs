﻿using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace LoggingFramework
{
    public class RawLogRenderer : ILogRenderer
    {
        public RawLogRenderer(string filepath)
        {
            _filepath = filepath;
        }

        public void Render(Logger logger)
        {
            // Create file
            _sw = new StreamWriter(_filepath);
            _currHierachy = 0;

            // Write Header
            _sw.WriteLine("Time|Hierachy|Name|Title|Desc|NonStandardAttribs");
            // TODO Do we need to add more to the header?

            // Iterate over logArray, writing the items in it
            foreach (var logItem in logger.LogArray)
            {
                RenderLogItem(logItem);
            }

            // Save to "filepath"
            _sw.Dispose();
        }

        public void RenderLogItem(LogItem logItem)
        {
            switch(logItem.Hierarchy)
            {
                case LogHierarchy.NewLevel:
                    _currHierachy += 1;
                    break;

                case LogHierarchy.SameLevel:
                    // No change
                    break;

                case LogHierarchy.EndLevel:
                    _currHierachy -= 1;
                    break;
            }
            
            var str = string.Format("{0}|{1}|{2}|{3}|{4}|",
                logItem.Time.ToString(),
                _currHierachy,
                logItem.Name,
                logItem.Title,
                logItem.Desc
                );

            str += string.Join(",", logItem.NonStandardAttributes.Select(kv => kv.Key + "=" + kv.Value));

            _sw.WriteLine(str);
        }

        string _filepath;
        StreamWriter _sw;
        int _currHierachy;
    }
}