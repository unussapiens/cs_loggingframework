﻿using System.Xml.Linq;

namespace LoggingFramework
{
    public class XmlLogRenderer : ILogRenderer
    {
        public XmlLogRenderer(string filepath)
        {
            _filepath = filepath;
        }

        public void Render(Logger logger)
        {
            // Create file
            _doc = new XDocument(new XElement("testReport"));
            _currentParent = _doc.Root;

            // Iterate over logArray, writing the items in it
            foreach (var logItem in logger.LogArray)
            {
                RenderLogItem(logItem);
            }

            // Save to "filepath"
            _doc.Save(_filepath);
        }

        public void RenderLogItem(LogItem logItem)
        {
            XElement new_node =
                new XElement(logItem.Name,
                new XAttribute("time", logItem.Time),
                new XAttribute("title", logItem.Title),
                new XAttribute("desc", logItem.Desc)
                );

            foreach (var item in logItem.NonStandardAttributes)
            {
                new_node.Add(new XAttribute(item.Key, item.Value));
            }

            // Set the parent for the next node
            switch (logItem.Hierarchy)
            {
                case LogHierarchy.NewLevel:
                    _currentParent.Add(new_node);
                    _currentParent = new_node;
                    break;

                case LogHierarchy.SameLevel:
                    _currentParent.Add(new_node);
                    break;

                case LogHierarchy.EndLevel:
                    _currentParent = _currentParent.Parent;
                    break;
            }
        }

        string _filepath;

        XDocument _doc;
        XElement _currentParent;
    }
}