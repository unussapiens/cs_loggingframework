﻿using System;
using System.Collections.Generic;

namespace LoggingFramework
{
    public enum TestStepVerdict
    {
        Pass,
        Fail,
        Warn,
        Info
    }

    public enum TestStepCategory
    {
        Primary,
        Secondary,
        Debug
    }

    public class TestStep : LogItem
    {
        public TestStep(TestStepVerdict verdict, TestStepCategory category, string title, string desc) : base()
        {
            this.Verdict = verdict;
            this.Category = category;
            this.Title = title;
            this.Desc = desc;
        }
        public override LogHierarchy Hierarchy { get { return LogHierarchy.SameLevel; } }

        public TestStepVerdict Verdict
        {
            get
            {
                Enum.TryParse(NonStandardAttributes["verdict"], out TestStepVerdict verdict);
                return verdict;
            }
            set
            {
                NonStandardAttributes["verdict"] = value.ToString();
            }
        }

        public TestStepCategory Category
        {
            get
            {
                Enum.TryParse(NonStandardAttributes["category"], out TestStepCategory category);
                return category;
            }
            set
            {
                NonStandardAttributes["category"] = value.ToString();
            }
        }

        public override string Name { get { return "testStep"; } }
    }
}
