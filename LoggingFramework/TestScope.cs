﻿using System;
using System.Collections.Generic;

namespace LoggingFramework
{
    public class TestScope : IDisposable
    {
        public TestScope(Logger logger, string title, string desc, bool collapsed)
        {
            _logger = logger;
            _logger.Log(new TestScopeStart(title, desc, collapsed));
        }
        public void Dispose()
        {
            _logger.Log(new TestScopeEnd());
        }

        Logger _logger;
    }

    class TestScopeStart : LogItem
    {
        public TestScopeStart(string title, string desc, bool collapsed) : base()
        {
            this.Title = title;
            this.Desc = desc;
            this.Collapsed = collapsed;
        }

        public override string Name { get { return "testScope"; } }
        public override LogHierarchy Hierarchy { get { return LogHierarchy.NewLevel; } }

        public bool Collapsed
        {
            get
            {
                return bool.Parse(NonStandardAttributes["collapsed"]);
            }
            set
            {
                NonStandardAttributes["collapsed"] = value.ToString();
            }
        }
    }

    class TestScopeEnd : LogItem
    {
        public override string Name { get { return "testScope"; } }
        public override LogHierarchy Hierarchy { get { return LogHierarchy.EndLevel; } }
    }
}
