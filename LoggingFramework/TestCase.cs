﻿using System;
using System.Collections.Generic;

namespace LoggingFramework
{
    public class TestCase : IDisposable
    {
        public TestCase(Logger logger, string testCaseId, string title, string description)
        {
            _logger = logger;
            _logger.Log(new TestCaseStart(testCaseId, title, description));
        }
        public void Dispose()
        {
            _logger.Log(new TestCaseEnd());
        }

        Logger _logger;
    }

    class TestCaseStart : LogItem
    {
        public TestCaseStart(string testCaseId, string title, string desc) : base()
        {
            this.TestCaseId = testCaseId;
            this.Title = title;
            this.Desc = desc;
        }

        public override string Name { get { return "testCase"; } }
        public override LogHierarchy Hierarchy { get { return LogHierarchy.NewLevel; } }

        public string TestCaseId
        {
            get
            {
                return NonStandardAttributes["testCaseId"];
            }
            set
            {
                NonStandardAttributes["testCaseId"] = value;
            }
        }
    }

    class TestCaseEnd : LogItem
    {
        public override string Name { get { return "testCase"; } }
        public override LogHierarchy Hierarchy { get { return LogHierarchy.EndLevel; } }
    }
}
