﻿using System.Collections.Generic;

namespace LoggingFramework
{
    public enum LogHierarchy
    {
        NewLevel = 1,   //! Create a new parent node
        SameLevel = 0,  //! Use the current parent node
        EndLevel = -1,  //! End the current parent node
    }

    public abstract class LogItem
    {
        public LogItem()
        {
            // TODO Enter the current time
            Name = "";
            Hierarchy = LogHierarchy.SameLevel;
            Time = 0;
            Title = "";
            Desc = "";
            NonStandardAttributes = new Dictionary<string, string>();
        }
        public virtual string Name { get; set; }
        public virtual LogHierarchy Hierarchy { get; set; }

        public int Time { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }

        public Dictionary<string, string> NonStandardAttributes { get; set; }
    }
}
